package be.kdg.shareit.application;

import be.kdg.shareit.domain.gebruiker.Gebruiker;
import be.kdg.shareit.business.GebruikerService;
import be.kdg.shareit.business.GereedschapsService;
import be.kdg.shareit.domain.gereedschap.state.Beschikbaar;
import be.kdg.shareit.domain.gereedschap.state.Geleend;
import be.kdg.shareit.domain.reservatie.Reservatie;
import be.kdg.shareit.domain.reservatie.ReservatieService;
import be.kdg.shareit.business.SharePointService;
import be.kdg.shareit.domain.transactie.AfhalingsTransactie;
import be.kdg.shareit.domain.transactie.TransactieLijn;
import be.kdg.shareit.business.TransactieService;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class TransactieController {

    private GebruikerService gebruikerService;
    private GereedschapsService gereedschapsService;
    private ReservatieService reservatieService;
    private TransactieService transactieService;
    private SharePointService sharePointService;

    private LocalDate date;

    public TransactieController() {
        this.gebruikerService = GebruikerService.getInstance();
        this.gereedschapsService = GereedschapsService.getInstance();
        this.reservatieService = ReservatieService.getInstance();
        this.transactieService = TransactieService.getInstance();
        this.sharePointService = SharePointService.getInstance(gebruikerService);
    }

    public GebruikerService getGebruikerController() {
        return gebruikerService;
    }

    public GereedschapsService getGereedschapsController() {
        return gereedschapsService;
    }

    public ReservatieService getReservatieController() {
        return reservatieService;
    }

    public TransactieService getTransactieSerivce() {
        return transactieService;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    //Gebruiker
    public Gebruiker getGebruiker(String naam) {
        return gebruikerService.getGebruiker(naam);
    }


    //Gereedschap


    //Reservatie

    //Scenario 1
    public List<Reservatie> toonAfTeHalenGereedschap(LocalDate date, String aanbiederNaam, String ontlenerNaam) {
        List<Reservatie> reservatiesForDate = new ArrayList<>();
        Gebruiker aanbieder = gebruikerService.getGebruiker(aanbiederNaam);
        Gebruiker ontlener = gebruikerService.getGebruiker(ontlenerNaam);

        for (Reservatie r : reservatieService.getReservatiesForOntlener(aanbieder, ontlener, date)
        ) {
            if (r.getStartDatum().equals(date)) reservatiesForDate.add(r);

        }

        return reservatiesForDate;

    }


    //Transactie

    //Scenario 2

    public void startOntlening(Gebruiker aanbieder, Gebruiker ontlener, LocalDate datum){
        reservatieService.getReservatiesForOntlener(aanbieder, ontlener, datum);
        AfhalingsTransactie afhalingsTransactie = transactieService.maakAfhalingstransactie(reservatieService.getReservatiesForOntlener(aanbieder, ontlener, datum), aanbieder, ontlener);

        for (Reservatie res: afhalingsTransactie.getReservaties()
             ) {

            if(!res.isGeannuleerd()){
                handleRes(res, afhalingsTransactie);
                handleWaarborg(res, afhalingsTransactie);
            } else {
                handleAnnulatie(res, afhalingsTransactie);
            }

        }
    }

    public double handleRes(Reservatie res, AfhalingsTransactie afhalingsTransactie) {


            double prijs = res.getTotaalPrijs();
            TransactieLijn transPrijs = transactieService.maakTransactieLijn(prijs, res);

            //Ontlener saldo verminderen
            sharePointService.aftrekken(transPrijs.getHoeveelheid().getWaarde(), res.getOntlener(), LocalDateTime.now());
            System.out.println(transPrijs.getHoeveelheid().getWaarde());


            //Aanbieder saldo opwaarderen
            sharePointService.optellen(transPrijs.getHoeveelheid().getWaarde(), res.getAanbieder(), LocalDateTime.now());


            //State op uitgeleend zetten
            res.getGereedschap().setGereedschapState(new Geleend(res.getGereedschap()));

            return prijs;


    }

    public double handleWaarborg(Reservatie res, AfhalingsTransactie afhalingsTransactie) {

        double prijs = res.getGereedschap().getWaarborg().getPrijs().getWaarde();

        TransactieLijn transPrijs = transactieService.maakTransactieLijn(prijs, afhalingsTransactie.getReservatie(res.getId()));
        sharePointService.aftrekken(transPrijs.getHoeveelheid().getWaarde(), res.getOntlener(), LocalDateTime.now());
        System.out.println(transPrijs.getHoeveelheid().getWaarde());
        sharePointService.optellen(transPrijs.getHoeveelheid().getWaarde(), res.getAanbieder(), LocalDateTime.now());

        return prijs;
    }


    public double handleAnnulatie(Reservatie res,AfhalingsTransactie afhalingsTransactie){

        Gebruiker vergoeder;
        Gebruiker vergoedde;


        if (res.getGeannuleerdDoor().getNaam().equals(res.getAanbieder().getNaam())) {
            vergoedde = res.getOntlener();
            vergoeder = res.getAanbieder();
        } else {
            vergoedde = res.getAanbieder();
            vergoeder = res.getOntlener();
        }

        double vergoeding = sharePointService.handleAnnulatie(res, vergoedde, vergoeder, LocalDateTime.now());
        TransactieLijn transVergoeding = transactieService.maakTransactieLijn(vergoeding, afhalingsTransactie.getReservatie(res.getId()));


        //State terug op beschikbaar zetten
        res.getGereedschap().setGereedschapState(new Beschikbaar(res.getGereedschap()));
        return vergoeding;
    }

    public boolean setGeannuleerd(Reservatie res, Gebruiker annuleerder) {

        Reservatie teAnnuleren = reservatieService.getReservatie(res.getId()); //res

        if (!teAnnuleren.isGeannuleerd()) {
            reservatieService.annuleerReservatie(res, annuleerder);
            return true;
        }

        return false;
    }


}
