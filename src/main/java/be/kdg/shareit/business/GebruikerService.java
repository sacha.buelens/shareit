package be.kdg.shareit.business;

import be.kdg.shareit.domain.gebruiker.Gebruiker;
import be.kdg.shareit.persistence.GebruikerRepo;
import be.kdg.shareit.persistence.IGebruikerRepo;
import be.kdg.shareit.domain.gereedschap.Gereedschap;
import be.kdg.shareit.domain.reservatie.Reservatie;

public class GebruikerService {

    private IGebruikerRepo gebruikerRepo;

    private GebruikerService() {
        this.gebruikerRepo = GebruikerRepo.getInstance();
    }

    public static GebruikerService getInstance() {
        return new GebruikerService();
    }

    public Gebruiker getGebruiker(String naam){
        return this.gebruikerRepo.getGebruiker(naam);
    }

    public boolean addGebruiker(Gebruiker g){
        return gebruikerRepo.addGebruiker(g);
    }

    public boolean addToReservaties(Gebruiker ontlener, Reservatie reservatie){
       if(ontlener.getReservaties().contains(reservatie)){
           return false;
       } else {
           ontlener.addToReservaties(reservatie);
           return true;
       }
    }

    public boolean addToAanbod(Gebruiker aanbieder, Gereedschap gereedschap){
        if(aanbieder.getAanbod().contains(gereedschap)){
            return false;
        } else {
            aanbieder.addToAanbod(gereedschap);
            return true;
        }
    }

    public void updateGebruikerSaldo(Gebruiker gebruiker, double hoeveelheid){
            gebruikerRepo.updateGebruikerSaldo(gebruiker, hoeveelheid);
    }
}
