package be.kdg.shareit.business;

import be.kdg.shareit.domain.gebruiker.Gebruiker;
import be.kdg.shareit.domain.gereedschap.Gereedschap;
import be.kdg.shareit.persistence.GereedschapRepo;
import be.kdg.shareit.persistence.IGereedschapRepo;

import java.util.List;

public class GereedschapsService {
    private IGereedschapRepo gereedschapRepo;

    private GereedschapsService() {
        this.gereedschapRepo = GereedschapRepo.getInstance();
    }

    public static GereedschapsService getInstance() {
        return new GereedschapsService();
    }

    public void addGereedschap(Gereedschap gereedschap) {
        gereedschapRepo.addGereedschap(gereedschap);
    }

    public List<Gereedschap> getGereedschappen(Gebruiker aanbieder) {
        return gereedschapRepo.getGereedSchappen(aanbieder);
    }

    public Gereedschap getGereedschap(String naam) {
        return gereedschapRepo.getGereedschap(naam);
    }
}
