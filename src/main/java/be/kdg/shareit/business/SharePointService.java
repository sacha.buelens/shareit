package be.kdg.shareit.business;

import be.kdg.shareit.domain.gebruiker.Gebruiker;
import be.kdg.shareit.domain.reservatie.Reservatie;
import be.kdg.shareit.domain.sharepoint.ISharePointVerdeler;
import be.kdg.shareit.domain.sharepoint.SharePoint;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class SharePointService implements ISharePointVerdeler {
    private GebruikerService gebruikerService;
    private static final String TIJD_FORMAAT = "dd-MM-yyyy HH:mm:ss";
    private static final double VERGOEDING = 0.1;

    private SharePointService(GebruikerService gebruikerService){
        this.gebruikerService = gebruikerService;
    }

    public static SharePointService getInstance(GebruikerService gebruikerService) {
        return new SharePointService(gebruikerService);
    }


    @Override
    public void optellen(double hoeveelheid, Gebruiker gebruiker, LocalDateTime tijdstip) {
        double spWaarde = gebruiker.getSaldoSP().getWaarde();

        spWaarde += hoeveelheid;

        gebruikerService.updateGebruikerSaldo(gebruiker, spWaarde);

        //be.kdg.shareit.domain.gebruiker.setSaldoSP(spWaarde);

        System.out.println(String.format("[ %s ]: Gebruiker %s heeft %.2f SharePoints verkregen bij zijn saldo.",
                tijdstip.format(DateTimeFormatter.ofPattern(TIJD_FORMAAT)), gebruiker.getNaam(), hoeveelheid));
    }

    @Override
    public void aftrekken(double hoeveelheid, Gebruiker gebruiker, LocalDateTime tijdstip) {
        double spWaarde = gebruiker.getSaldoSP().getWaarde();

        spWaarde -= hoeveelheid;

        gebruikerService.updateGebruikerSaldo(gebruiker, spWaarde);
        //be.kdg.shareit.domain.gebruiker.setSaldoSP(spWaarde);

        System.out.println(String.format("[ %s ]: Gebruiker %s betaalde %.2f SharePoints van zijn saldo.",
                tijdstip.format(DateTimeFormatter.ofPattern(TIJD_FORMAAT)), gebruiker.getNaam(), hoeveelheid));
    }

    @Override
    public SharePoint BerekenTotaal(Gebruiker gebruiker, LocalDateTime tijdstip) {
        System.out.println(String.format("[ %s ]: Gebruiker %s zijn totaal aantal SharePoints zijn %.2f.", tijdstip.format(DateTimeFormatter.ofPattern(TIJD_FORMAAT)), gebruiker.getNaam(), gebruiker.getSaldoSP().getWaarde()));
        return gebruiker.getSaldoSP();
    }

    @Override
    public double handleAnnulatie(Reservatie reservatie, Gebruiker vergoeder, Gebruiker vergoedde, LocalDateTime tijdstip) {
        double teBetalenVergoeding = reservatie.getTotaalPrijs() * VERGOEDING;

        //Trek 10% van kost af
        if (tijdstip.isAfter(reservatie.getStartDatum().atStartOfDay()) && tijdstip.isBefore(reservatie.getStartDatum().plusDays(6).atStartOfDay())){
            this.aftrekken(teBetalenVergoeding, vergoeder, tijdstip);
            this.optellen(teBetalenVergoeding, vergoedde, tijdstip);
        }

        System.out.println(String.format("[ %s ]: Annulatie van be.kdg.shareit.domain.reservatie afgehandeld.\nAanbieder: %s\nOntlener: %s", tijdstip.format(DateTimeFormatter.ofPattern(TIJD_FORMAAT)), vergoedde.getNaam(), vergoeder.getNaam()));
        return teBetalenVergoeding;
    }
}
