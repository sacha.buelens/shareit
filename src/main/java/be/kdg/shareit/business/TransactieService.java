package be.kdg.shareit.business;

import be.kdg.shareit.domain.gebruiker.Gebruiker;
import be.kdg.shareit.domain.reservatie.Reservatie;
import be.kdg.shareit.domain.transactie.AfhalingsTransactie;
import be.kdg.shareit.persistence.ITransactieRepo;
import be.kdg.shareit.domain.transactie.TransactieLijn;
import be.kdg.shareit.persistence.TransactieRepo;

import java.util.List;

public class TransactieService {

    private ITransactieRepo transactieRepo;

    private TransactieService() {
        this.transactieRepo = TransactieRepo.getInstance();
    }

    public static TransactieService getInstance() {
        return new TransactieService();
    }

    public void voerTransactieUit(TransactieLijn transactieLijn) {

    }

    public AfhalingsTransactie maakAfhalingstransactie(List<Reservatie> reservaties, Gebruiker aanbieder, Gebruiker ontlener) {

        AfhalingsTransactie afhalingsTransactie = new AfhalingsTransactie(reservaties, aanbieder, ontlener);
        transactieRepo.addAfhalingstransactie(afhalingsTransactie);
        return afhalingsTransactie;
    }

    public TransactieLijn maakTransactieLijn(double hoeveelheid, Reservatie res) {
        TransactieLijn transLijn = new TransactieLijn(res, hoeveelheid);
        transactieRepo.addTransactieLijn(res, hoeveelheid);
        return transLijn;
    }

    public AfhalingsTransactie getAfhalingsTransactie(Reservatie res) {
        return transactieRepo.getAfhalingsTransactie(res);
//        return transactieRepo.getAfhalingsTransactie(res).getTransacties();
    }

}
