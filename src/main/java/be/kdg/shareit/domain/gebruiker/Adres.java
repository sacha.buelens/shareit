package be.kdg.shareit.domain.gebruiker;

public class Adres {
    private String straat;
    private int huisnr;
    private int zipcode;
    private String gemeente;
    private String land;

    public Adres() {
        this.straat = "Eenstraat";
        this.huisnr = 1;
        this.zipcode = 2000;
        this.gemeente = "Eengemeente";
        this.land = "Eenland";
    }


    public Adres(String straat, int huisnr, int zipcode, String gemeente, String land) {
        this.straat = straat;
        this.huisnr = huisnr;
        this.zipcode = zipcode;
        this.gemeente = gemeente;
        this.land = land;
    }
}
