package be.kdg.shareit.domain.gebruiker;

import be.kdg.shareit.domain.gereedschap.Gereedschap;
import be.kdg.shareit.domain.reservatie.Reservatie;
import be.kdg.shareit.domain.sharepoint.SharePoint;
import be.kdg.shareit.business.SharePointService;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Gebruiker {
    private SharePoint saldoSP;
    private String naam;
    private List<Gereedschap> aanbod;
    private List<Reservatie> reservaties;
    private SharePointService verdeler;

    public Gebruiker() { }

    public Gebruiker(SharePoint saldoSP, String naam) {
        this.saldoSP = saldoSP;
        this.naam = naam;
        this.aanbod = new ArrayList<>();
        this.reservaties = new ArrayList<>();

    }

    public SharePoint getSaldoSP() {
        return saldoSP;
    }

    public void setSaldoSP(double saldoSP) {
        this.saldoSP.setWaarde(saldoSP);
    }

    public String getNaam() {
        return naam;
    }

    public void addToAanbod(Gereedschap gereedschap) {
        aanbod.add(gereedschap);
    }

    public List<Gereedschap> getAanbod() {
        return aanbod;
    }

    public void reserveer(Gereedschap gereedschap,String id, int periode, LocalDate startDatum) {
        Reservatie newRes = new Reservatie(id,periode, gereedschap.getAanbieder(), this, startDatum, gereedschap);
        reservaties.add(newRes);
    }

    public void addToReservaties(Reservatie res) {
        reservaties.add(res);
    }

    public List<Reservatie> getReservaties() {
        return reservaties;
    }


}
