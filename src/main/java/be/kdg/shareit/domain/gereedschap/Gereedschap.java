package be.kdg.shareit.domain.gereedschap;

import be.kdg.shareit.domain.gebruiker.Gebruiker;
import be.kdg.shareit.domain.gereedschap.state.GereedschapState;
import be.kdg.shareit.domain.gereedschap.state.NietGekeurd;
import be.kdg.shareit.domain.sharepoint.SharePoint;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Gereedschap implements GereedschapComponent {
    private String naam;
    private String beschrijving;
    private Type type;
    private SharePoint prijs;
    private List<Onderdeel> onderdelen;
    private Gebruiker aanbieder;
    private Waarborg waarborg;
    private GereedschapState state;



    public Gereedschap(String naam,String beschrijving,Type type){
        this.naam = naam;
        this.beschrijving = beschrijving;
        this.type = type;
        this.state = new NietGekeurd(this);
        this.prijs = new SharePoint(0);
    }

    public Gereedschap(String naam, String beschrijving, Type type,int prijs, Gebruiker aanbieder, Waarborg waarborg) {
        this.naam = naam;
        this.beschrijving = beschrijving;
        this.type = type;
        this.prijs = new SharePoint(prijs);
        this.aanbieder = aanbieder;
        onderdelen = new ArrayList<>();
        this.waarborg = waarborg;
        this.state = new NietGekeurd(this);
    }


    public String getNaam() {
        return naam;
    }

    public String getBeschrijving() {
        return beschrijving;
    }

    public Type getType() {
        return type;
    }

    public Gebruiker getAanbieder() {
        return aanbieder;
    }

    public SharePoint getPrijs() {
        return prijs;
    }

    public Waarborg getWaarborg() {
        return waarborg;
    }

    public void addOnderdeel(Onderdeel onderdeel){
        onderdelen.add(onderdeel);
    }

    public List<Onderdeel> getOnderdelen() {
        return onderdelen;
    }

    public void setGereedschapState(GereedschapState state){
        this.state = state;

        for (GereedschapComponent child : onderdelen
             ) {
            child.setGereedschapState(state);
        }
    }


    public void keurGereedschap() {
        this.state.keurGereedschap(true);
    }


    public void stelBeschikbaar() {
        this.state.stelBeschikbaar();
    }


    public void stelNietBeschikbaar() {
        this.state.stelNietBeschikbaar();
    }


    public void ontvangGereedschap() {
        this.state.ontvangGereedschap(LocalDate.now(), 1);
    }


    public void reserveer() {
        this.state.reserveer();
    }


    public void leenUit() {
        this.state.leenUit();
    }

    @Override
    public SharePoint getTotaalPrijs() {


        double totaalPrijs;
        if(onderdelen.size() == 0){
            totaalPrijs = prijs.getWaarde();
        } else {
            totaalPrijs = 0;
            for (GereedschapComponent child: onderdelen
            ) {
                totaalPrijs += child.getTotaalPrijs().getWaarde();
            }
        }

        return new SharePoint(totaalPrijs);
    }

    @Override
    public Waarborg getTotaalWaarborg() {
        return waarborg;
    }
}
