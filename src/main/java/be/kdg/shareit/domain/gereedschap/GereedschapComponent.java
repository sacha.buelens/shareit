package be.kdg.shareit.domain.gereedschap;

import be.kdg.shareit.domain.gereedschap.state.GereedschapState;
import be.kdg.shareit.domain.sharepoint.SharePoint;

public interface GereedschapComponent {
    SharePoint getTotaalPrijs();
    Waarborg getTotaalWaarborg();
    void setGereedschapState(GereedschapState state);
}
