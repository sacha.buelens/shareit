package be.kdg.shareit.domain.gereedschap;

import be.kdg.shareit.domain.gereedschap.state.GereedschapState;
import be.kdg.shareit.domain.sharepoint.SharePoint;

public class Onderdeel implements GereedschapComponent {
    private String naam;
    private String beschrijving;
    private Gereedschap gereedschap;
    private SharePoint prijs;
    private Waarborg waarborg;
    private GereedschapState state;

    public Onderdeel(){
        this.naam = "Default";
        this.beschrijving = "Default beschrijving";
        this.gereedschap = null;
        this.prijs = new SharePoint(0);
        this.waarborg = new Waarborg(Type.TUIN);
    }

    public Onderdeel(String naam, String beschrijving, Gereedschap gereedschap, int prijs, Waarborg waarborg) {
        this.naam = naam;
        this.beschrijving = beschrijving;
        this.gereedschap = gereedschap;
        this.prijs = new SharePoint(prijs);
        this.waarborg = waarborg;
    }

    @Override
    public SharePoint getTotaalPrijs() {
        return prijs;
    }

    @Override
    public Waarborg getTotaalWaarborg() {
        return waarborg;
    }

    @Override
    public void setGereedschapState(GereedschapState state) {
        this.state = state;
    }
}
