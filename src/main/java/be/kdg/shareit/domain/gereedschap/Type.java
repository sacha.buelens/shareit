package be.kdg.shareit.domain.gereedschap;

public enum Type {

    BOUW(40),KEUKEN(60), GELUID(400), LICHT(150), TUIN(50), WERK(5);

    public final int value;

    Type(int value){
        this.value = value;
    }
}
