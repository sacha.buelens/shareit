package be.kdg.shareit.domain.gereedschap;
import be.kdg.shareit.domain.sharepoint.SharePoint;

public class Waarborg {
    private SharePoint prijs;
    private Type type;

    public Waarborg(Type type) {
        this.type = type;
        this.prijs = new SharePoint(type.value);
    }

    public SharePoint getPrijs() {
        return prijs;
    }

    public Type getType() {
        return type;
    }
}
