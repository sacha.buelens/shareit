package be.kdg.shareit.domain.gereedschap.state;

import be.kdg.shareit.domain.gereedschap.Gereedschap;

public class Beschikbaar extends GereedschapState {

    public Beschikbaar(Gereedschap gereedschap) {
        super(gereedschap);
    }


    @Override
    public void stelNietBeschikbaar() throws IllegalStateException {
        gereedschap.setGereedschapState(new NietBeschikbaar(gereedschap));
    }

    @Override
    public void reserveer() throws IllegalStateException {

        gereedschap.setGereedschapState(new Gereserveerd(gereedschap));

    }

}
