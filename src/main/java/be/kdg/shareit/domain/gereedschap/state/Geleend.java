package be.kdg.shareit.domain.gereedschap.state;

import be.kdg.shareit.domain.gereedschap.Gereedschap;

import java.time.LocalDate;

public class Geleend extends GereedschapState {

    public Geleend(Gereedschap gereedschap) {
        super(gereedschap);
    }

    @Override
    public void ontvangGereedschap(LocalDate startDate, int periode) throws IllegalStateException {
        if(LocalDate.now().isEqual(LocalDate.of(startDate.getYear(), startDate.getMonth(), startDate.getDayOfMonth() + periode))){
            gereedschap.setGereedschapState(new Beschikbaar(gereedschap));
        }
    }
}
