package be.kdg.shareit.domain.gereedschap.state;

import be.kdg.shareit.domain.gereedschap.Gereedschap;

import java.time.LocalDate;

public abstract class GereedschapState {
    protected Gereedschap gereedschap;

    public GereedschapState(Gereedschap gereedschap) {
        this.gereedschap = gereedschap;
    }

    public void keurGereedschap(boolean gekeurd) throws IllegalStateException{
        throw new IllegalStateException();
    }

    public void stelBeschikbaar()throws IllegalStateException{
        throw new IllegalStateException();
    }

    public void stelNietBeschikbaar()throws IllegalStateException{
        throw new IllegalStateException();
    }

    public void ontvangGereedschap(LocalDate startDate, int periode)throws IllegalStateException{
        throw new IllegalStateException();
    }

    public void reserveer()throws IllegalStateException{
        throw new IllegalStateException();
    }

    public void leenUit()throws IllegalStateException{
        throw new IllegalStateException();
    }




}
