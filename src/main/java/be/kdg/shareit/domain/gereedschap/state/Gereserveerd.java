package be.kdg.shareit.domain.gereedschap.state;


import be.kdg.shareit.domain.gereedschap.Gereedschap;

public class Gereserveerd extends GereedschapState {

    public Gereserveerd(Gereedschap gereedschap) {
        super(gereedschap);
    }

    @Override
    public void leenUit() throws IllegalStateException {
        gereedschap.setGereedschapState(new Geleend(gereedschap));
    }
}
