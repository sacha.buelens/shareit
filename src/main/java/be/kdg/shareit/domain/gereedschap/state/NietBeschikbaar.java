package be.kdg.shareit.domain.gereedschap.state;

import be.kdg.shareit.domain.gereedschap.Gereedschap;

public class NietBeschikbaar extends GereedschapState {

    public NietBeschikbaar(Gereedschap gereedschap) {
        super(gereedschap);
    }

    @Override
    public void stelBeschikbaar() throws IllegalStateException {
       gereedschap.setGereedschapState(new Beschikbaar(gereedschap));
    }
}
