package be.kdg.shareit.domain.gereedschap.state;

import be.kdg.shareit.domain.gereedschap.Gereedschap;

public class NietGekeurd extends GereedschapState {


    public NietGekeurd(Gereedschap gereedschap) {
        super(gereedschap);
    }

    @Override
    public void keurGereedschap(boolean geaccepteerd) throws IllegalStateException {
        if(geaccepteerd){
            gereedschap.setGereedschapState(new Gekeurd(gereedschap));
        }

    }
}
