package be.kdg.shareit.domain.reservatie;

import be.kdg.shareit.domain.gebruiker.Gebruiker;
import be.kdg.shareit.domain.gereedschap.Gereedschap;

import java.time.LocalDate;

public class Reservatie {
    private String id;
    private int periode;
    private double totaalPrijs; //Prijs + waarborg
    private Gebruiker aanbieder;
    private Gebruiker ontlener;
    private LocalDate startDatum;
    private boolean geannuleerd;
    private Gebruiker geannuleerdDoor;
    private Gereedschap gereedschap;

    public Reservatie(String id, int periode, Gebruiker aanbieder, Gebruiker ontlener, LocalDate startDatum, Gereedschap gereedschap) {
        this.id = id;
        this.periode = periode;
        this.totaalPrijs = 0;
        this.aanbieder = aanbieder;
        this.ontlener = ontlener;
        this.startDatum = startDatum;
        this.geannuleerd = false;
        this.gereedschap = gereedschap;
        this.geannuleerdDoor = new Gebruiker();
    }

    public String getId() {
        return this.id;
    }

    public void setAnnulatie(boolean annulatie, Gebruiker geannuleerdDoor) {

        geannuleerd = annulatie;
        this.geannuleerdDoor = geannuleerdDoor;
    }

    public void calculateTotal() {
        totaalPrijs += gereedschap.getPrijs().getWaarde() + gereedschap.getWaarborg().getPrijs().getWaarde();
    }

    public int getPeriode() {
        return periode;
    }

    public double getTotaalPrijs() {
        return totaalPrijs +=(gereedschap.getPrijs().getWaarde() * periode);
    }

    public Gebruiker getAanbieder() {
        return aanbieder;
    }

    public Gebruiker getOntlener() {
        return ontlener;
    }

    public LocalDate getStartDatum() {
        return startDatum;
    }

    public boolean isGeannuleerd() {
        return geannuleerd;
    }

    public Gereedschap getGereedschap() {
        return gereedschap;
    }

    public Gebruiker getGeannuleerdDoor() {
        return geannuleerdDoor;
    }
}
