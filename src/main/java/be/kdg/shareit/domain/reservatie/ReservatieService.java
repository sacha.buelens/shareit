package be.kdg.shareit.domain.reservatie;

import be.kdg.shareit.domain.gebruiker.Gebruiker;
import be.kdg.shareit.application.TransactieController;
import be.kdg.shareit.persistence.IReservatieRepo;
import be.kdg.shareit.persistence.ReservatieRepo;

import java.time.LocalDate;
import java.util.List;

public class ReservatieService {
    private IReservatieRepo reservatieRepo;
    private TransactieController transactieController;


    private ReservatieService() {
        this.reservatieRepo = ReservatieRepo.getInstance();
    }

    public static ReservatieService getInstance() {
        return new ReservatieService();
    }

    public List<Reservatie> getReservatiesForOntlener(Gebruiker aanbieder, Gebruiker ontlener, LocalDate date) {
        return reservatieRepo.getReservaties(aanbieder, ontlener,date);
    }

    public void addReservatie(Reservatie res) {
        reservatieRepo.addReservatie(res);

    }

    public Reservatie getReservatie(String id) {
        return reservatieRepo.getReservatie(id);
    }

    public Reservatie getReservatieForOntlener(Gebruiker aanbieder, Gebruiker ontlener) {
        return reservatieRepo.getReservatie(ontlener);
    }

    public void startOntlening(Gebruiker ontlener, Reservatie reservatie, LocalDate startDatum) {


    }

    public void annuleerReservatie(Reservatie reservatie, Gebruiker geannuleerdDoor) {

        reservatieRepo.annuleerReservatie(reservatie, geannuleerdDoor);
    }
}
