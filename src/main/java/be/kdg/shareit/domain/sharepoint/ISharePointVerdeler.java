package be.kdg.shareit.domain.sharepoint;

import be.kdg.shareit.domain.gebruiker.Gebruiker;
import be.kdg.shareit.domain.reservatie.Reservatie;

import java.time.LocalDateTime;

public interface ISharePointVerdeler {
    void optellen(double hoeveelheid, Gebruiker gebruiker, LocalDateTime tijdstip);
    void aftrekken(double hoeveelheid, Gebruiker gebruiker, LocalDateTime tijdstip);
    SharePoint BerekenTotaal(Gebruiker gebruiker, LocalDateTime tijdstip);
    double handleAnnulatie(Reservatie reservatie, Gebruiker ontlener, Gebruiker aanbieder, LocalDateTime tijdstip);
}
