package be.kdg.shareit.domain.sharepoint;

public class SharePoint {
     private double waarde;

    public SharePoint(double waarde) {
        this.waarde = waarde;
    }

    public double getWaarde() {
        return waarde;
    }

    public void setWaarde(double waarde) {
        this.waarde = waarde;
    }
}
