package be.kdg.shareit.domain.transactie;

import be.kdg.shareit.domain.gebruiker.Gebruiker;
import be.kdg.shareit.domain.reservatie.Reservatie;

import java.util.ArrayList;
import java.util.List;

public class AfhalingsTransactie {
    private Gebruiker aanbieder, ontlener;
    private List<Reservatie> reservaties;
    private List<TransactieLijn> transacties;

    public AfhalingsTransactie(List<Reservatie> reservaties, Gebruiker aanbieder, Gebruiker ontlener){
        this.reservaties = reservaties;
        this.aanbieder = aanbieder;
        this.ontlener = ontlener;
        transacties = new ArrayList<>();
    }

    public List<Reservatie> getReservaties() {
        return reservaties;
    }

    public Reservatie getReservatie(String id){
        for (Reservatie r: reservaties) {
            if(r.getId().equals(id)) return r;

        }
        return null;
    }

    public Gebruiker getAanbiederVanAfhalingstransactie() {
        return aanbieder;
    }

    public Gebruiker getOntlenerVanAfhalingstransactie() {
        return ontlener;
    }

    public List<TransactieLijn> getTransacties() {
        return transacties;
    }

    public void addTransactieLijn(TransactieLijn lijn){
        transacties.add(lijn);
    }
}
