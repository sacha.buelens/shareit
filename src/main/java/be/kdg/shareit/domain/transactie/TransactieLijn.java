package be.kdg.shareit.domain.transactie;

import be.kdg.shareit.domain.reservatie.Reservatie;
import be.kdg.shareit.domain.sharepoint.SharePoint;

public class TransactieLijn {
    private Reservatie reservatie;
    private SharePoint hoeveelheid;

    public TransactieLijn(Reservatie reservatie, double hoeveelheid) {
        this.reservatie = reservatie;
        this.hoeveelheid = new SharePoint(hoeveelheid);
    }

    public Reservatie getReservatie() {
        return reservatie;
    }

    public SharePoint getHoeveelheid() {
        return hoeveelheid;
    }
}
