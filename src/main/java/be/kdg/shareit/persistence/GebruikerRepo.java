package be.kdg.shareit.persistence;

import be.kdg.shareit.domain.gebruiker.Gebruiker;

import java.util.ArrayList;
import java.util.List;

public class GebruikerRepo implements IGebruikerRepo {
    private List<Gebruiker> gebruikers;

    private GebruikerRepo() {
        this.gebruikers = new ArrayList<>();
    }

    public static GebruikerRepo getInstance() {
        return new GebruikerRepo();
    }

    @Override
    public List<Gebruiker> getGebruikers() {
        return gebruikers;
    }

    @Override
    public Gebruiker getGebruiker(String naam) {
        for (Gebruiker g : gebruikers
        ) {
            if (g.getNaam().equals(naam)) return g;
        }
        return null;
    }

    @Override
    public boolean addGebruiker(Gebruiker gebruiker) {
        if (getGebruiker(gebruiker.getNaam()) == null) {
            gebruikers.add(gebruiker);
            return true;
        }
        return false;

    }

    @Override
    public void updateGebruikerSaldo(Gebruiker gebruiker, double hoeveelheid) {
        getGebruiker(gebruiker.getNaam()).setSaldoSP(hoeveelheid);
    }
}
