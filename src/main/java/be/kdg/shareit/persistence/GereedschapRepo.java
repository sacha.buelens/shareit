package be.kdg.shareit.persistence;

import be.kdg.shareit.domain.gebruiker.Gebruiker;
import be.kdg.shareit.domain.gereedschap.Gereedschap;

import java.util.ArrayList;
import java.util.List;

public class GereedschapRepo implements IGereedschapRepo {

    private List<Gereedschap> gereedschapList;

    private GereedschapRepo() {
        gereedschapList = new ArrayList<>();
    }

    public static GereedschapRepo getInstance() {
        return new GereedschapRepo();
    }

    @Override
    public void addGereedschap(Gereedschap gereedschap){
        gereedschapList.add(gereedschap);
    }

    @Override
    public List<Gereedschap> getGereedSchappen(Gebruiker aanbieder){
        List<Gereedschap> forAanbieder = new ArrayList<>();

        for (Gereedschap g: gereedschapList
             ) {
            if(g.getAanbieder().getNaam().equals(aanbieder.getNaam())){
                forAanbieder.add(g);
            }

        }

        return forAanbieder;
     }
    @Override
    public Gereedschap getGereedschap(String naam){
        for (Gereedschap g: gereedschapList
        ) {
            if(g.getNaam().equals(naam)) return g;

        }
        return null;
    }
}
