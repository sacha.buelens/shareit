package be.kdg.shareit.persistence;

import be.kdg.shareit.domain.gebruiker.Gebruiker;

import java.util.List;

public interface IGebruikerRepo {
    List<Gebruiker> getGebruikers();

    Gebruiker getGebruiker(String naam);

    boolean addGebruiker(Gebruiker gebruiker);

    void updateGebruikerSaldo(Gebruiker gebruiker, double hoeveelheid);
}
