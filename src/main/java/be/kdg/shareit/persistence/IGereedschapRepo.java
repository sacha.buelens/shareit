package be.kdg.shareit.persistence;

import be.kdg.shareit.domain.gebruiker.Gebruiker;
import be.kdg.shareit.domain.gereedschap.Gereedschap;

import java.util.List;

public interface IGereedschapRepo {
    void addGereedschap(Gereedschap gereedschap);

    List<Gereedschap> getGereedSchappen(Gebruiker aanbieder);

    Gereedschap getGereedschap(String naam);
}
