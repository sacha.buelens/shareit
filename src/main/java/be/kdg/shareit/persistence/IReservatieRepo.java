package be.kdg.shareit.persistence;

import be.kdg.shareit.domain.gebruiker.Gebruiker;
import be.kdg.shareit.domain.reservatie.Reservatie;

import java.time.LocalDate;
import java.util.List;

public interface IReservatieRepo {
    void addReservatie(Reservatie reservatie);

    Reservatie getReservatie(Gebruiker ontlener);

    Reservatie getReservatie(String id);

    List<Reservatie> getReservaties(Gebruiker aanbieder, Gebruiker ontlener, LocalDate date);

    List<Reservatie> getReservaties();
    boolean annuleerReservatie(Reservatie res, Gebruiker annuleerder);
}
