package be.kdg.shareit.persistence;

import be.kdg.shareit.domain.reservatie.Reservatie;
import be.kdg.shareit.domain.transactie.AfhalingsTransactie;

public interface ITransactieRepo {
    void addAfhalingstransactie(AfhalingsTransactie afhalingsTransactie);

    void addTransactieLijn(Reservatie reservatie, double hoeveelheid);

    AfhalingsTransactie getAfhalingsTransactie(Reservatie reservatie);
}
