package be.kdg.shareit.persistence;

import be.kdg.shareit.domain.gebruiker.Gebruiker;
import be.kdg.shareit.domain.reservatie.Reservatie;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ReservatieRepo implements IReservatieRepo {
    private List<Reservatie> reservaties;

    private ReservatieRepo() {
        this.reservaties = new ArrayList<>();
    }

    public static ReservatieRepo getInstance() {
        return new ReservatieRepo();
    }

    @Override
    public void addReservatie(Reservatie reservatie) {
        reservaties.add(reservatie);
    }

    @Override
    public Reservatie getReservatie(Gebruiker ontlener) {
        for (Reservatie res : reservaties
        ) {

            if (res.getOntlener().getNaam().equals(ontlener.getNaam())) {
                return res;
            }

        }
        return null;
    }

    @Override
    public Reservatie getReservatie(String id) {
        for (Reservatie res : reservaties
        ) {

            if (res.getId().equals(id)) {
                return res;
            }

        }
        return null;
    }


    @Override
    public List<Reservatie> getReservaties(Gebruiker aanbieder, Gebruiker ontlener, LocalDate date) {
        List<Reservatie> tmpRes = new ArrayList<>();

        for (Reservatie res : reservaties
        ) {
            if (res.getAanbieder() == aanbieder) {
                if (res.getOntlener() == ontlener) {
                    if (res.getStartDatum().equals(date))
                        //if(res.getOntlener().getNaam().equals(ontlener.getNaam())){
                        tmpRes.add(res);
                }
            }
        }

        return tmpRes;
    }

    @Override
    public List<Reservatie> getReservaties() {
        return reservaties;
    }

    public boolean annuleerReservatie(Reservatie reservatie, Gebruiker annuleerder){

        if(!getReservatie(reservatie.getId()).isGeannuleerd()){
            getReservatie(reservatie.getId()).setAnnulatie(true, annuleerder);
            return true;
        }

        return false;

    }
}
