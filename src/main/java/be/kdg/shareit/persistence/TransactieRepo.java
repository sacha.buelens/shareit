package be.kdg.shareit.persistence;

import be.kdg.shareit.domain.reservatie.Reservatie;
import be.kdg.shareit.domain.transactie.AfhalingsTransactie;
import be.kdg.shareit.domain.transactie.TransactieLijn;

import java.util.ArrayList;
import java.util.List;

public class TransactieRepo implements ITransactieRepo {
    private List<AfhalingsTransactie> afhalingsTransacties;

    private TransactieRepo() {
        this.afhalingsTransacties = new ArrayList<>();
    }

    public static TransactieRepo getInstance() {
        return new TransactieRepo();
    }

    @Override
    public void addAfhalingstransactie(AfhalingsTransactie afhalingsTransactie){
        afhalingsTransacties.add(afhalingsTransactie);
    }

    @Override
    public void addTransactieLijn(Reservatie reservatie, double hoeveelheid){
        getAfhalingsTransactie(reservatie).addTransactieLijn(new TransactieLijn(reservatie, hoeveelheid));

    }

    @Override
    public AfhalingsTransactie getAfhalingsTransactie(Reservatie reservatie){

        for (AfhalingsTransactie a: afhalingsTransacties
             ) {
            if(
                    a.getAanbiederVanAfhalingstransactie().getNaam().equals(reservatie.getAanbieder().getNaam()) && a.getOntlenerVanAfhalingstransactie().getNaam().equals(reservatie.getOntlener().getNaam())){
                return a;
            }
        }

        return null;
    }


}
