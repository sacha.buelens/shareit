package tests;

import be.kdg.shareit.domain.gebruiker.Gebruiker;
import be.kdg.shareit.domain.gebruiker.GebruikerService;
import be.kdg.shareit.domain.gereedschap.*;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.jupiter.api.Assertions;
import be.kdg.shareit.domain.reservatie.Reservatie;
import be.kdg.shareit.domain.reservatie.ReservatieService;
import be.kdg.shareit.application.TransactieController;
import be.kdg.shareit.domain.sharepoint.SharePoint;
import be.kdg.shareit.domain.transactie.AfhalingsTransactie;
import be.kdg.shareit.domain.transactie.TransactieService;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


/**
 * Ciaran Vinken
 * 24/11/2019
 */
public class MyStepdefs {
    private Gebruiker g1;
    private Gebruiker g2;
    private Reservatie testRes;
    private AfhalingsTransactie trans1;
    private Gebruiker annulatieGeb;

    private TransactieController tc = new TransactieController();
    private GebruikerService gs = tc.getGebruikerController();
    private ReservatieService rs = tc.getReservatieController();
    private GereedschapsService grs = tc.getGereedschapsController();
    private TransactieService ts = tc.getTransactieSerivce();
    private LocalDate date;

    @Given("Gebruiker omschrijvingen")
    public void gebruikerOmschrijvingen(DataTable dataTable) {
        for (Map<String, String> map : dataTable.asMaps()) {
            gs.addGebruiker(new Gebruiker(new SharePoint(Integer.parseInt(map.get("saldoSP"))), map.get("naam")));
        }
        g1 = gs.getGebruiker("Eveline");
        g2 = gs.getGebruiker("Diederik");
    }

    @Given("Gereedschap omschrijvingen")
    public void gereedschapOmschrijvingen(DataTable dataTable) {
        for (Map<String, String> map : dataTable.asMaps()) {
            grs.addGereedschap(new Gereedschap(map.get("naam"), map.get("beschrijving"), Type.valueOf(map.get("type")),
                    Integer.parseInt(map.get("prijs")), gs.getGebruiker(map.get("aanbieder")),
                    new Waarborg(Type.valueOf(map.get("type")))));
        }
    }

    @Given("Onderdeel omschrijvingen")
    public void onderdeelOmschrijvingen(DataTable dataTable) {
        for (Map<String, String> map : dataTable.asMaps()) {
            Gereedschap tempGer = grs.getGereedschap(map.get("gereedschap"));
            tempGer
                    .addOnderdeel(new Onderdeel(map.get("naam"), map.get("beschrijving"), tempGer,
                            Integer.parseInt(map.get("prijs")), new Waarborg(grs.getGereedschap(map.get("gereedschap")).getType())));
        }
    }

    @Given("Reservatie omschrijvingen")
    public void reservatieOmschrijvingen(DataTable dataTable) {
        for (Map<String, String> map : dataTable.asMaps()) {
            LocalDate tmpDate = LocalDate.parse(map.get("startdatum"), DateTimeFormatter.ofPattern("dd-MM-yyyy"));
            rs.addReservatie(new Reservatie(map.get("id"), Integer.parseInt(map.get("periode")), gs.getGebruiker(map.get("aanbieder")),
                    gs.getGebruiker(map.get("ontlener")), tmpDate, grs.getGereedschap(map.get("gereedschap"))));
        }
    }


    @Given("today is {string}")
    public void today_is(String string) {
        date = LocalDate.parse(string, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        System.out.println(date);
        Assertions.assertEquals(LocalDate.of(2019, 12, 15), date);
    }


    @When("{string} aangeeft dat {string} zijn reservaties wil ophalen")
    public void aangeeftDatZijnReservatiesWilOphalen(String arg0, String arg1) {
        rs.getReservatiesForOntlener(gs.getGebruiker(arg0), gs.getGebruiker(arg1), date);
    }

    @Then("wordt een lijst met {int} reservaties getoond")
    public void wordtEenLijstMetReservatiesGetoond(Integer arg0) {
        assertEquals("Fout aantal reservaties", arg0,
                Integer.valueOf(rs.getReservatiesForOntlener(g1, g2, date).size()));
    }

    @Then("bevat de lijst {string} en {string}")
    public void bevatDeLijstEn(String arg0, String arg1) {
        for (Reservatie res : rs.getReservatiesForOntlener(g1, g2, date)) {
            System.out.println(res.toString() + " Res id: " + res.getId());
        }
        Reservatie testRes1 = rs.getReservatie(arg0);
        assertNotNull(testRes1);
        Reservatie testRes2 = rs.getReservatie(arg1);
        assertNotNull(testRes2);
    }

//SCENARIO 2

    @When("{string} aangeeft dat {string} {string} wil ophalen")
    public void aangeeftDatWilOphalen(String arg0, String arg1, String arg2) {
        g1 = gs.getGebruiker(arg0);
        g2 = gs.getGebruiker(arg1);
        testRes = rs.getReservatie(arg2);
        assertNotNull(gs.getGebruiker(arg0));
        assertNotNull(gs.getGebruiker(arg1));
        assertNotNull(rs.getReservatie(arg2));
    }

    @And("{string} aangeeft dat {string} {string} wil annuleren")
    public void aangeeftDatWilAnnuleren(String arg0, String arg1, String arg2) {
        annulatieGeb = gs.getGebruiker(arg1);
        assertNotNull(annulatieGeb);
        assertNotNull(gs.getGebruiker(arg0));
        assertNotNull(gs.getGebruiker(arg1));
        assertNotNull(rs.getReservatie(arg2));
    }

    @Then("wordt er een afhalingtransactie aangemaakt")
    public void wordtErEenAfhalingtransactieAangemaakt() {
        trans1 = ts.maakAfhalingstransactie(rs.getReservatiesForOntlener(g1, g2, date), g1, g2);
        assertNotNull(trans1);
    }

    @And("Er wordt een transactielijn aangemaakt voor de ontlening {string} met een prijs van {int} SP")
    public void erWordtEenTransactielijnAangemaaktVoorDeOntleningMetEenPrijsVanSP(String arg0, int arg1) {
        double test = tc.handleRes(rs.getReservatie(arg0), trans1);
        assertEquals(arg1, (int) test);
    }

    @And("Er wordt een transactielijn gemaakt voor de waarborg van {string} met een prijs van {int} SP")
    public void erWordtEenTransactielijnGemaaktVoorDeWaarborgVanMetEenPrijsVanSP(String arg0, int arg1) {
        double test = tc.handleWaarborg(rs.getReservatie(arg0), trans1);
        assertEquals(arg1, (int) test);
    }

    @And("er wordt een transactielijn gemaakt voor de annulatie van {string} met een prijs van {int} SP")
    public void erWordtEenTransactielijnGemaaktVoorDeAnnulatieVanMetEenPrijsVanSP(String arg0, int arg1) {
        tc.setGeannuleerd(rs.getReservatie(arg0), g2);
        System.out.println(rs.getReservatie(arg0).isGeannuleerd());
        double test = tc.handleAnnulatie(rs.getReservatie(arg0),trans1);
        System.out.println(test);
        assertEquals(arg1,(int) test);
    }

    @And("heeft {string} {int} SP")
    public void heeftSP(String arg0, int arg1) {
        System.out.println(gs.getGebruiker(arg0).getNaam() + " heeft een saldo van: " + gs.getGebruiker(arg0).getSaldoSP().getWaarde());
        Assertions.assertEquals(arg1, gs.getGebruiker(arg0).getSaldoSP().getWaarde());
    }


}
