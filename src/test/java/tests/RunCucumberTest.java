package tests;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;


/**
 * Ciaran Vinken
 * 22/11/2019
 */
//@RunWith(Cucumber.class)
@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"pretty", "summary"}, features = "src/test/resources/", glue= "java.tests")
public class RunCucumberTest {
}
