Feature: Toon af te halen gereedschap

  Background:
    Given Gebruiker omschrijvingen
      | saldoSP | naam     |
      | 1000    | Eveline  |
      | 1000    | Diederik |
      | 870     | Albert   |
    Given Gereedschap omschrijvingen
      | naam               | beschrijving                  | type   | prijs | aanbieder |
      | ZaagKit            | Dit is een kettingzaag        | TUIN   | 13    | Eveline   |
      | Kettingzaag2       | Dit is een kettingzaag        | TUIN   | 10   | Eveline   |
      | Keukenrobot        | Dit is een keubenrobot        | KEUKEN | 15    | Eveline   |
      | PA Seismic         | Dit is een geluidsinstallatie | GELUID | 200   | Albert    |
      | Black Strobo Magic | Dit is een lichtinstallatie   | LICHT  | 50    | Albert    |
    Given Onderdeel omschrijvingen
      | naam            | beschrijving               | gereedschap | prijs |
      | Kettingzaag     | Dit is een kettingzaag     | ZaagKit     | 10    |
      | Zaagbroek       | Dit is een zaagbroek       | ZaagKit     | 2     |
      | Veiligheidshelm | Dit is een veiligheidshelm | ZaagKit     | 1     |
    Given Reservatie omschrijvingen
      | id    | periode | aanbieder | ontlener | startdatum | gereedschap  |
      | Res1  | 3       | Eveline   | Diederik | 15-12-2019 | ZaagKit      |
      | Res2  | 4       | Eveline   | Diederik | 15-12-2019 | Kettingzaag2 |
      | Res3  | 2       | Eveline   | Diederik | 17-12-2019 | Keukenrobot  |
      | Res4  | 2       | Albert    | Diederik | 15-12-2019 | PA Seismic   |
      | Res10 | 2       | Albert    | Eveline  | 30-12-2019 | PA Seismic   |

  Scenario: toon af te halen gereedschap
    Given today is "15-12-2019"
    When "Eveline" aangeeft dat "Diederik" zijn reservaties wil ophalen
    Then wordt een lijst met 2 reservaties getoond
    And bevat de lijst "Res1" en "Res2"

  Scenario: ontlening met annulatie
    Given today is "15-12-2019"
    When "Eveline" aangeeft dat "Diederik" "Res1" wil ophalen
    And "Eveline" aangeeft dat "Eveline" "Res2" wil annuleren
    Then wordt er een afhalingtransactie aangemaakt
    And Er wordt een transactielijn aangemaakt voor de ontlening "Res1" met een prijs van 39 SP
    And Er wordt een transactielijn gemaakt voor de waarborg van "Res1" met een prijs van 50 SP
    And er wordt een transactielijn gemaakt voor de annulatie van "Res2" met een prijs van 4 SP
    And heeft "Diederik" 915 SP
    And heeft "Eveline" 1085 SP
